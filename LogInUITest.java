package com.epam.marathon.ui;

import com.epam.marathon.UITestBase;
import lombok.extern.slf4j.Slf4j;
import com.epam.marathon.upamers.bo.HomePageBO;
import com.epam.marathon.upamers.bo.LandingPageBO;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

@Test
@Slf4j
public class LogInUITest extends UITestBase {

    private LandingPageBO landingPageBO;
    private HomePageBO homePageBO;

    @BeforeMethod
    public void pageInit() {
        landingPageBO = new LandingPageBO();
        homePageBO = new HomePageBO();
    }

    @Test(description = "Log In test", dataProvider = "Login Data data provider")
    public void loginTest(String login, String pwd) {
        landingPageBO.logIn(login, pwd);
        Assert.assertTrue(homePageBO.isAvatarHeaderButtonDisplayed(), "Avatar header icon is not displayed");
    }

    @DataProvider(name = "Login Data data provider")
    public Object [][] loginData() {
        return new Object [][] {
            {kit2023@mailinator.com, Test12345@},
            {pes@mailinator.com, Test12345@},
        };
    }

    @Test(description = "Verify all elements are displayed on the Home page")
    public void homePageTest() {
        Assert.assertTrue(homePageBO.isSearchFildDisplayed(),"Search fild is not displayed on the Home page");
        Assert.assertTrue(homePageBO.isFilterButtonDisplayed(),"Filter button is not displayed on the Home page");
        Assert.assertFalse(homePageTest.isContactsListEmpty(), "Some contacts should be displayed on the Home page");
        Assert.assertTrue(homePageBO.isChatButtonDisplayed(),"Chat button is not displayed on the Home page");
    }

// Взагалі покищо не знайома з автоматичним тестуванням, тому на жаль не зможу написати тест для перевірки роботи фільтрування.

}
